<?php
/**
 * @file
 * smc_syllabus_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function smc_syllabus_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_syllabus:syllabus
  $menu_links['main-menu_syllabus:syllabus'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'syllabus',
    'router_path' => 'syllabus',
    'link_title' => 'Syllabus',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_syllabus:syllabus',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Syllabus');


  return $menu_links;
}
